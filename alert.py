import logging
import requests
import json
import simpleaudio as sa
import time
import os
from datetime import datetime

def now():
    now = datetime.now()
    return now.strftime("%d/%m/%Y %H:%M:%S")

def check_server_down():
    try:
        r = requests.get("https://www.google.com/", timeout=15) 
        return r.status_code != 200
    except:
        return True


def wait_for_internet():
    while True:
        if not check_server_down():
            logging.info("%s - Network back up.", now())
            break
        else:
            logging.error("Network still down.")
            time.sleep(10)


def main_loop():
    x = 0
    logging.info("Starting to check for internet...")
    while True:
        if check_server_down():
            x = x + 1
            logging.info("%s - (%s/6) - Network problems detected.", now(), x)
            if x >= 6:
                logging.warning("%s - Network went down.", now())
                # play_alert()
                wait_for_internet()
                x = 0
            else:
                time.sleep(5)
        else:
            if x > 0:
                x = 0
                logging.info("Network normalized")
            time.sleep(5)    


filename = 'warning-alarm-buzzer.wav'
wave_obj = sa.WaveObject.from_wave_file(filename)


def play_alert():
    play_obj = wave_obj.play()
    play_obj.wait_done()

if __name__ == '__main__':
    logging.basicConfig()
    logging.root.setLevel(logging.INFO)
    main_loop()
